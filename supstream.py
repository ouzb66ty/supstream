import cv2
import socket
import threading
import sys
import os
import time

class RTSPClient(threading.Thread):

    def __init__(self, channel, url):
        threading.Thread.__init__(self)
        self.channel = channel
        self.url = url
        self.stream = cv2.VideoCapture(url)
        print(self.channel + ' RTSP channel started')

    def run(self):
        while(1):
            ret = self.stream.grab()

    def grab(self, filename):
        ret, frame = self.stream.retrieve()
        while ret == False:
            ret, frame = self.stream.retrieve()
        cv2.imwrite(filename, frame)
        print(self.channel + ' RTSP channel grab')

    def close(self):
        self.stream.release()
        print(self.channel + ' RTSP channel closed')

if __name__ == "__main__":

    if len(sys.argv) == 2:
        head = []
        port = int(sys.argv[1])
        socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        socket.bind(('', port))
        socket.listen(1)
        client, address = socket.accept()
        print('New user connected')
        while 1:
            response = client.recv(300).decode('ascii').rstrip("\n\r")
            splitRes = response.split(' ')
            if len(splitRes) == 3 and splitRes[0] == 'new':
                stream = RTSPClient(splitRes[1], splitRes[2])
                stream.start()
                head.append(stream)
                time.sleep(2)
                client.send(b'READY')
            elif len(splitRes) == 3 and splitRes[0] == 'grab':
                i = 0
                while i < len(head):
                    if head[i].channel == splitRes[1]:
                        head[i].grab(os.path.abspath(splitRes[2]))
                        client.send(b'OK')
                        break
                    i += 1
            elif len(splitRes) == 1 and splitRes[0] == 'chan':
                i = 0
                while i < len(head):
                    print(head[i].channel)
                    i += 1
            elif len(splitRes) == 2 and splitRes[0] == 'close':
                i = 0
                while i < len(head):
                    if head[i].channel == splitRes[1]:
                        head[i].close()
                        break
                    i += 1
    else:
        print('usage: python3 app.py <port>');
        print('- new : launch RTSP connection')
        print('\tnew <channel> <url>')
        print('- grab : capture one frame')
        print('\tgrab <channel> <filename>')
        print('- chan : get list of all RTSP channel')
        print('\tchan')
        print('- close : close RTSP connection')
        print('\tclose <channel>')